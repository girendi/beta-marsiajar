package com.d3ti.betamarsiajar.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.d3ti.betamarsiajar.R;
import com.d3ti.betamarsiajar.model.User;

import static com.d3ti.betamarsiajar.utils.MyApp.dbUser;

public class RegisterActivity extends AppCompatActivity {

    private EditText etName, etUsername, etPassword;
    private Spinner spRole;
    private Button btnRegister;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        declarationLayout();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEditText();
            }
        });
    }

    private void checkEditText() {
        String name = etName.getText().toString();
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        String role = spRole.getSelectedItem().toString();
        if (name.isEmpty()){
            Toast.makeText(this, "Please input your name", Toast.LENGTH_SHORT).show();
        }else if (username.isEmpty()){
            Toast.makeText(this, "Please input your username", Toast.LENGTH_SHORT).show();
        }else if (password.isEmpty()){
            Toast.makeText(this, "Please input your password", Toast.LENGTH_SHORT).show();
        }else if (role.isEmpty()){
            Toast.makeText(this, "Please Choose your role", Toast.LENGTH_SHORT).show();
        }else{
            user = new User();
            user.setName(name);
            user.setUsername(username);
            user.setPassword(password);
            user.setRole(role);
            dbUser.userDao().insertUser(user);
            Toast.makeText(this, "Akun Berhasil dibuat!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        }
    }

    private void declarationLayout(){
        etName = findViewById(R.id.etName);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        spRole = findViewById(R.id.spRole);
        btnRegister = findViewById(R.id.btnRegister);
    }

    public void viewLogin(View view) {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }
}
