package com.d3ti.betamarsiajar.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.d3ti.betamarsiajar.R;
import com.d3ti.betamarsiajar.model.User;

import java.util.List;

import static com.d3ti.betamarsiajar.utils.MyApp.dbUser;

public class LoginActivity extends AppCompatActivity {

    private EditText etUsername, etPassword;
    private Button btnLogin;
    private List<User> users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        declarationLayout();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEditText();
            }
        });
    }

    private void checkEditText() {
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        if (username.isEmpty()){
            Toast.makeText(this, "Please input your username", Toast.LENGTH_SHORT).show();
        }else if (password.isEmpty()){
            Toast.makeText(this, "Please input your password", Toast.LENGTH_SHORT).show();
        }else {
            users = dbUser.userDao().getUserLogin(username, password);
            if (users.isEmpty()){
                Toast.makeText(this, "User tidak ditemukan", Toast.LENGTH_SHORT).show();
            }else{
                for (User user : users){
                    if (user.getRole().equals("User")){
                        Toast.makeText(this, "Login User Sukses " + user.getName(), Toast.LENGTH_SHORT).show();
                        break;
                    }else{
                        Toast.makeText(this, "Login Admin Sukses " + user.getName(), Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
            }
        }
    }

    private void declarationLayout(){
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
    }

    public void viewRegister(View view) {
        startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
        finish();
    }
}
