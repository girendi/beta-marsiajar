package com.d3ti.betamarsiajar.utils;

import android.app.Application;

import androidx.room.Room;

public class MyApp extends Application {
    public static UserDatabase dbUser;

    @Override
    public void onCreate() {
        super.onCreate();
        dbUser = Room.databaseBuilder(getApplicationContext(),
                UserDatabase.class, "user").allowMainThreadQueries().build();
    }
}
