package com.d3ti.betamarsiajar.utils;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.d3ti.betamarsiajar.model.User;

import java.util.List;

@Dao
public interface UserDao {

    @Query("Select * FROM User")
    List<User> getAllUser();

    @Query("Select * From User Where username = :username And password = :password")
    List<User> getUserLogin(String username, String password);

    @Insert
    void insertUser(User... users);
}
