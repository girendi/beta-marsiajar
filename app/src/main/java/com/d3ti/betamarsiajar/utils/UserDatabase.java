package com.d3ti.betamarsiajar.utils;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.d3ti.betamarsiajar.model.User;

@Database(entities = {User.class}, version = 1)
public abstract class UserDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}
